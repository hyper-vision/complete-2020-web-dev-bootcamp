const express = require("express");
const bodyParser = require("body-parser");
const request = require("request");

app = express();
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/index.html");
});

app.post("/", (req, res) => {
  const crypto = req.body.crypto;
  const fiat = req.body.fiat;
  const amount = req.body.amount;
  // const symbol = req.body.crypto + req.body.fiat;
  const symbol_set = "global";

  // const url = `https://apiv2.bitcoinaverage.com/indices/${symbol_set}/ticker/${symbol}`;
  const url = `https://apiv2.bitcoinaverage.com/convert/${symbol_set}?from=${crypto}&to=${fiat}&amount=${amount}`;

  const options = {
    url: `https://apiv2.bitcoinaverage.com/convert/${symbol_set}`,
    method: "GET",
    qs: {
      from: crypto,
      to: fiat,
      amount: amount
    }
  };
  request(options, (error, response, body) => {
    if (error) {
      console.log("error:", error);
      res.redirect("/");
    }
    console.log("response:", response && response.statusCode);
    if (response.statusCode == 200) {
      const responseBody = JSON.parse(body);
      const date = responseBody.time;
      const price = responseBody.price;

      res.write(`<p>The current date is <strong> ${date} </strong></p>`);
      res.write(
        `The current price for <strong>${amount} ${crypto}</strong> in <strong>${fiat}</strong> is <strong>${price} ${fiat}<strong>`
      );
      res.send();
    } else {
      console.log(response.body);
      res.redirect("/");
    }
  });
});

app.listen(3000, () => {
  console.log("listening on 3000");
});
