const express = require("express");
const bodyParser = require("body-parser");
const request = require("request");

app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static("public")); // configure express to serve static files

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/signup.html");
});

app.get("/failure", (req, res) => {
  res.sendFile(__dirname + "/failure.html");
});

app.post("/failure", (req, res) => {
  res.redirect("/");
});

app.get("/success", (req, res) => {
  res.sendFile(__dirname + "/success.html");
});

app.post("/success", (req, res) => {
  res.redirect("/");
});

app.post("/", (req, res) => {
  const first_name = req.body.first_name;
  const last_name = req.body.last_name;
  const email = req.body.email;

  const data = {
    members: [
      {
        email_address: email,
        status: "subscribed",
        merge_fields: {
          FNAME: first_name,
          LNAME: last_name
        }
      }
    ]
  };

  const jsonData = JSON.stringify(data);

  const options = {
    url: "https://us4.api.mailchimp.com/3.0/lists/e4d8d69c36",
    method: "POST",
    // authentication
    headers: {
      Authorization: "red 18bd7420a844815697b753a8fab200b4-us4"
    }
    // content
    // body: jsonData
  };

  request(options, (error, response, body) => {
    if (error) {
      res.redirect("/failure");
    } else if (response.statusCode === 200) {
      res.redirect("/success");
    } else {
      res.redirect("/failure");
    }
  });
});

app.listen(3000, () => {
  console.log("Listening on port 3000");
});

// Mailchimp API key
// 18bd7420a844815697b753a8fab200b4-us4

// List ID
// e4d8d69c36
