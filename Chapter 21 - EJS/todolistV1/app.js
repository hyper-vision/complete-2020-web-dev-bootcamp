const express = require("express");
const bodyParser = require("body-parser");

const numToDays = {
  0: "Sunday",
  1: "Monday",
  2: "Tuesday",
  3: "Wednesday",
  4: "Thursday",
  5: "Friday",
  6: "Saturday"
};

app = express();
app.use(bodyParser.urlencoded({ extended: true }));
// Initialize EJS
app.set("view engine", "ejs");

app.get("/", (req, res) => {
  const today = new Date();
  const day = today.getDay();

  res.render("list", {
    day: numToDays[day],
    day2: ["Electric Boogaloo", "Electric Boogashoo", "Electric Boogaroo"]
  });
});

app.listen(3000, () => {
  console.log("listening on port 3000");
});
