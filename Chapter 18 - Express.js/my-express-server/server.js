const express = require("express");
const app = express();
const port = 3000;

// Method that defines what should happen when someone makes a GET request to the home route
// The callback function defines the logic that could execute when the request is sent
app.get("/", (req, res) => {
  //   console.log(req);
  res.send("<h2>hello world!</h2>");
});

app.get('/contact', (req, res) => {
  res.send("Contact me at oisjcoseijc@gmail.com");
});

app.get('/about', (req, res) => {
  res.send("This is my personal site")
})

app.listen(port, () => console.log(`example app listening on port ${port}!`));
