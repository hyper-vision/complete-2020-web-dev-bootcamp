const express = require("express");
const bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/bmicalculator", (req, res) => {
  res.sendFile(__dirname + "/index.html");
});

app.post("/bmicalculator", (req, res) => {
  const height = Number(req.body.height) / 100;
  const weight = Number(req.body.weight);

  const bmi = weight / height ** 2;

  res.send(`Your BMI is: ${bmi}`);
});

app.listen(3000, () => {
  console.log(`Listening on Port: 3000`);
});
