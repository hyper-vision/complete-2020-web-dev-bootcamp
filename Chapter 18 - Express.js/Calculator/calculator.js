const express = require("express");
const bodyParser = require("body-parser"); // To parse post request data

const app = express();
const port = 3000;

app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  return res.sendFile(__dirname + "/index.html");
});

app.post("/", (req, res) => {
  // HTTP data is sent and recieved as string
  const sum = Number(req.body.num1) + Number(req.body.num2);
  res.send(sum.toString());
});

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
